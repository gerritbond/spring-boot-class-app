Project: spring-boot-class-app
Author: Gerrit Bond <gerrit.bond@udig.com, gerritbond@gmail.com>

## Description
This project contains a walkthrough of building a spring application, on each 
branch is a different stage of development.

The final product is a basic spring application capable of fulfilling 
the problem statement given below.

## Requirements
- Install postgres and generate the database. Definition is found in resources/sql.

## Problem
- Students need a way to register for classes 
- Professors need a way to create a class
- Classes have a maximum class size that must be enforced
- The application must provide a JSON based API for integration into existing systems.

## Technology
- [Spring] (https://spring.io/)
- [Spring Boot] (https://projects.spring.io/spring-boot/)
- [Spring Initializr] (https://start.spring.io/)
- [Java 8] (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [jOOQ] (https://www.jooq.org/)

## Getting Started
- Clone the repository `git@gitlab.com:gerritbond/spring-boot-class-app.git`
- Checkout the problems branch `git checkout problems`
- Select a problem branch and check it out

### Spring initializr 
Contains the generated spring boot project from [Spring Initializr] (https://start.spring.io)

## Known Issues
