swagger: "2.0"
info:
  description: "Specification for a sample project using Spring Boot; details simple endpoints for manipulating Students, Professors, Teacher Assistants, Buildings, and Classes"
  version: "0.0.1"
  title: "Class App"
  contact:
    email: "gerrit.bond@udig.com"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host : "localhost:8080"
basePath: "/class-api/v1"
tags:
- name: "student"
  description: "Access information around students"
- name: "professor"
  description: "Access information around professors"
- name: "class"
  description: "Access information around classes"
- name: "school"
  description: "Access information around schools"

schemes:
- "http"
- "https"
paths:
  /student:
    post:
      tags:
        - "student"
      summary: "Register a new student"
      description: "Creates a new student given the specified parameters."
      operationId: "addStudent"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Student object to create"
          required: true
          schema:
            $ref: "#/definitions/CreateStudent"
      responses:
        200:
          description: "Success, created record"
          schema:
            $ref: "#/definitions/Student"
        409:
          description: "Conflict, cannot create record, potentially existant."
    put:
      tags:
        - "student"
      summary: "Update a student record"
      description: "Updates an existing student record. This operation is idempotent."
      operationId: "updateStudent"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Updated Student object"
          required: true
          schema:
            $ref: "#/definitions/Student"
      responses:
        200:
          description: "Success."
        404:
          description: "Conflict, cannot update record, potentitally non-existant"
    get:
      tags:
        - "student"
      summary: "Get list of students"
      description: "Get list of students"
      operationId: "getStudents"
      produces:
        - "application/json"
      responses:
        200:
          description: "Success."
          schema:
            $ref: "#/definitions/Students"

  /student/{studentId}:
    get:
      tags:
        - "student"
      summary: "Find student by student id"
      description: "Returns a student if they exist"
      operationId: "getStudentById"
      produces:
        - "application/json"
      parameters:
        - name: "studentId"
          in: "path"
          description: "Students id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/Student"
        404:
          description: "Student not found"
    delete:
      tags:
        - "student"
      summary: "Deletes student given their student id"
      description: "Deletes student given their student id"
      operationId: "deleteStudentById"
      produces:
        - "application/json"
      parameters:
        - name: "studentId"
          in : "path"
          description: "Students id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Success"
        404:
          description: "No such record"
  
  /student/{studentId}/enroll/{classId}:
    put:
      tags:
        - "student"
      summary: "Enrolls a student in a class if there is room."
      description: "Enrolls a student in a class if there is room."
      operationId: "enrollStudent"
      produces:
        - "application/json"
      parameters:
        - name: "studentId"
          in: "path"
          description: "Students id"
          required: true
          type: "integer"
          format: "int64"
        - name: "classId"
          in: "path"
          description: "Class id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Successfully enrolled."
        404:
          description: "Student or class not found"
  /student/{studentId}/disenroll/{classId}:
    put:
      tags:
        - "student"
      summary: "Disenrolls a student from a class."
      description: "Disenrolls a student from a class."
      operationId: "disenrollStudent"
      produces:
        - "application/json"
      parameters:
        - name: "studentId"
          in: "path"
          description: "Students id"
          required: true
          type: "integer"
          format: "int64"
        - name: "classId"
          in: "path"
          description: "Class id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Sucessfully disenrolled."
        404:
          description: "Student or class not found."


  /professor:
    post:
      tags:
        - "professor"
      summary: "Add a new professor"
      description: "Adds a new professor"
      operationId: "addProfessor"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Professor to create"
          required: true
          schema:
            $ref: "#/definitions/CreateProfessor"
      responses:
        200:
          description: "Success, created professor"
    get:
      tags:
        - "professor"
      summary: "Get a list of professors"
      description: "Gets a list of professors"
      operationId: "getProfessors"
      produces:
        - "application/json"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/Professors"
            
  /professor/{professorId}:
    get:
      tags:
        - "professor"
      summary: "Find a particular professor record"
      description: "Finds a particular professor given their id"
      operationId: "findProfessor"
      produces:
        - "application/json"
      parameters:
        - name: "professorId"
          in: "path"
          description: "Professors id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Sucess."
          schema:
            $ref: "#/definitions/Professor"
    put:
      tags:
        - "professor"
      summary: "Updates a professor record"
      description: "Updates a professor record"
      operationId: "updateProfessor"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - name: "professorId"
          in: "path"
          description: "Professors id"
          required: true
          type: "integer"
          format: "int64"
        - in: "body"
          name: "body"
          description: "Updated Professor Object"
          required: true
          schema:
            $ref: "#/definitions/CreateProfessor"
      responses:
        200:
          description: "Sucessfully updated."
        404:
          description: "Professor not found"
    delete:
      tags:
        - "professor"
      summary: "Deletes a given professor record"
      description: "Deletes a particular professor given their id"
      operationId: "deleteProfessor"
      parameters:
        - name: "professorId"
          in: "path"
          description: "Professors id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Sucessfully deleted."


  /school:
    post:
      tags:
        - "school"
      summary: "Create a new school"
      description: "Creates a new school given the schools name"
      operationId: "addSchool"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "School object to create"
          required: true
          schema:
            $ref: "#/definitions/CreateSchool"
      responses:
        200:
          description: "Success, created school"
        409:
          description: "Conflict, existing subject collides with this one."
    get:
      tags:
        - "school"
      summary: "Get all schools"
      description: "Returns a list of all schools"
      operationId: "getSchools"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/Schools"
  
  /school/{schoolId}:
    get:
      tags:
        - "school"
      summary: "Fetches a particular school"
      description: "Fetches a particular school"
      operationId: "getSchool"
      parameters:
        - name: "schoolId"
          in: "path"
          description: "School id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/School"
        404:
          description: "No such school found"
    delete:
      tags:
        - "school"
      summary: "Delete a particular school"
      description: "Delete a particular school"
      operationId: "deleteSchool"
      parameters:
        - name: "schoolId"
          in: "path"
          description: "School id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Success, school deleted"
        404:
          description: "No such school found"

    put:
      tags:
        - "school"
      summary: "Updates a particular school"
      description: "Updates a particular school"
      operationId: "updateSchool"
      parameters:
        - name: "schoolId"
          in: "path"
          description: "School id"
          required: true
          type: "integer"
          format: "int64"
        - name: "body"
          in: "body"
          description: "School Object"
          schema:
            $ref: "#/definitions/CreateSchool"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/School"
        404:
          description: "No such school found"



  /class:
    post:
      tags:
        - "class"
      summary: "Create a new class"
      description: "Creates a new class given a subject, and professor."
      operationId: "addClass"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Class object to create"
          required: true
          schema:
            $ref: "#/definitions/CreateClass"
      responses:
        200:
          description: "Success, created class"
        400:
          description: "Missing or invalid class information."
        409:
          description: "Conflict, cannot create record, potentially existant."
    put:
      tags:
        - "class"
      summary: "Update a class record"
      description: "Updates an existing class record. This operation is idempotent."
      operationId: "updateClass"
      consumes:
        - "application/json"
      produces:
        - "application/json"
      parameters:
        - in: "body"
          name: "body"
          description: "Updated class object"
          required: true
          schema:
            $ref: "#/definitions/Class"
      responses:
        200:
          description: "Success."
        404:
          description: "Conflict, cannot update record, potentitally non-existant"
    get:
      tags:
        - "class"
      summary: "Get list of all classes"
      description: "Get list of classes"
      operationId: "getClasses"
      produces:
        - "application/json"
      responses:
        200:
          description: "Success."
          schema:
            $ref: "#/definitions/Classes"
            
  /class/school/{schoolId}:
    get:
      tags:
        - "class"
      summary: "Get list of classes matching school"
      description: "Get list of classes matching school"
      operationId: "getClassesBySchool"
      produces:
        - "application/json"
      parameters:
        - name: "schoolId"
          in: "path"
          description: "School Id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/Classes"
            
  /class/professor/{professorId}:
    get:
      tags:
        - "class"
      summary: "Get list of classes matching a professor"
      description: "Get list of classes matching a professor"
      operationId: "getClassesByProfessor"
      produces:
        - "application/json"
      parameters:
        - name: "professorId"
          in: "path"
          description: "Professor Id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/Classes"
  /class/{classId}:
    get:
      tags:
        - "class"
      summary: "Get a class by id"
      description: "Get a class by id"
      operationId: "getClass"
      produces: 
        - "application/json"
      parameters:
        - name: "classId"
          in: "path"
          description: "Class Id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/Class"
    delete:
      tags:
        - "class"
      summary: "Delete a class by id"
      operationId: "deleteClass"
      produces: 
        - "application/json"
      parameters:
        - name: "classId"
          in: "path"
          description: "Class Id"
          required: true
          type: "integer"
          format: "int64"
      responses:
        200:
          description: "Success"

  /user:
    post:
      tags:
      - "user"
      summary: "Create user"
      operationId: "createUser"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Created user object"
        required: true
        schema:
          $ref: "#/definitions/User"
      responses:
        200:
          description: "Success"
  /user/login:
    get:
      tags:
      - "user"
      summary: "Logs user into the system"
      description: ""
      operationId: "loginUser"
      produces:
      - "application/json"
      parameters:
      - name: "username"
        in: "query"
        description: "The user name for login"
        required: true
        type: "string"
      - name: "password"
        in: "query"
        description: "The password for login in clear text"
        required: true
        type: "string"
      responses:
        200:
          description: "successful operation"
          schema:
            type: "string"
          headers:
            X-Expires-After:
              type: "string"
              format: "date-time"
              description: "date in UTC when token expires"
        400:
          description: "Invalid username/password supplied"
  /user/logout:
    get:
      tags:
      - "user"
      summary: "Logs out current logged in user session"
      description: ""
      operationId: "logoutUser"
      produces:
      - "application/json"
      parameters: []
      responses:
        default:
          description: "successful operation"

definitions:
  Student:
    type: "object"
    required:
      - "id"
    properties:
      id:
        type: "integer"
        format: "int64"
      first_name:
        type: "string"
      middle_name:
        type: "string"
      last_name:
        type: "string"
      address1:
        type: "string"
      address2:
        type: "string"
      school:
        $ref: "#/definitions/School"
      credits:
        type: "integer"
  CreateStudent:
    type: "object"
    required:
      - "first_name"
      - "last_name"
    properties:
      first_name:
        type: "string"
      middle_name:
        type: "string"
      last_name:
        type: "string"
      address1:
        type: "string"
      address2:
        type: "string"
      school:
        $ref: "#/definitions/School"
      credits:
        type: "integer"
  Students:
    type: "array"
    items: 
      $ref: "#/definitions/Student"
      
  Class:
    type: "object"
    required:
      - "id"
    properties:
      id:
        type: "integer"
        format: "int64"
      course_name:
        type: "string"
      professor:
        $ref: "#/definitions/Professor"
      school:
        $ref: "#/definitions/School"
      students:
        $ref: "#/definitions/Students"
  CreateClass:
    type: "object"
    required:
      - "course_name"
      - "course_number"
      - "subject"
      - "professor"
    properties:
      course_name:
        type: "string"
      professor:
        type: "integer"
        format: "int64"
      school:
        type: "integer"
        format: "int64"
  Classes:
    type: "array"
    items:
      $ref: "#/definitions/Class"

  Professor:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
      school:
        $ref: "#/definitions/School"
      first_name:
        type: "string"
      last_name:
        type: "string"
      middle_name:
        type: "string"
      title:
        type: "string"
  
  CreateProfessor:
    type: "object"
    required:
      - "first_name"
      - "last_name"
      - "middle_name"
      - "title"
    properties:
      school:
        $ref: "#/definitions/School"
      first_name:
        type: "string"
      last_name:
        type: "string"
      middle_name:
        type: "string"
      title:
        type: "string"
        
  Professors:
    type: "array"
    items:
      $ref: "#/definitions/Professor"

    
  School:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
      name:
        type: "string"
        
  CreateSchool:
    type: "object"
    required:
      - "name"
    properties:
      name:
        type: "string"
        
  Schools:
    type: "array"
    items:
      $ref: "#/definitions/School"
      
  User:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
      username:
        type: "string"
      firstName:
        type: "string"
      lastName:
        type: "string"
      password:
        type: "string"
      userStatus:
        type: "integer"
        format: "int32"
        description: "User Status"