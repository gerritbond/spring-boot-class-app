package com.udig.classapp.controllers;

import com.sun.org.apache.bcel.internal.generic.GETFIELD;
import com.udig.classapp.models.Class;
import com.udig.classapp.services.ClassService;
import com.udig.classapp.services.ServiceCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
@RequestMapping(value = "class-api/v1/class")
public class ClassController {

    private final ClassService classService;

    @Autowired
    public ClassController (ClassService classService) {
        this.classService = classService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Class> createClass (@RequestBody Class newClass) {
        ServiceCall<Class> serviceCall = classService.create(newClass);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());

    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Class>> getAllClasses () {
        ServiceCall<List<Class>> serviceCall = classService.find();

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/school/{schoolId}")
    public ResponseEntity<List<Class>> getClassesBySchool (@PathVariable(name = "schoolId") Integer schoolId) {
        ServiceCall<List<Class>> serviceCall = classService.findBySchool(schoolId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/professor/{professorId}")
    public ResponseEntity<List<Class>> getProfessorsBySchool (@PathVariable(name = "professorId") Integer professorId) {
        ServiceCall<List<Class>> serviceCall = classService.findByProfessor(professorId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{classId}")
    public ResponseEntity<Class> updateClass (@PathVariable(name = "classId") Integer classId, @RequestBody Class updatedClass) {
        ServiceCall<Class> serviceCall = classService.update(classId, updatedClass);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{classId}")
    public ResponseEntity<Class> getClass (@PathVariable(name = "classId") Integer classId) {
        ServiceCall<Class> serviceCall = classService.find(classId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{classId}")
    public ResponseEntity deleteClass (@PathVariable(name = "classId") Integer classId) {
        ServiceCall<Boolean> serviceCall = classService.delete(classId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }
}
