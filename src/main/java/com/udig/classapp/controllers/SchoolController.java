package com.udig.classapp.controllers;

import com.udig.classapp.models.School;
import com.udig.classapp.services.SchoolService;
import com.udig.classapp.services.ServiceCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping(value = "class-api/v1/school")
public class SchoolController {
    private final SchoolService schoolService;

    @Autowired
    public SchoolController (SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    @RequestMapping(method = POST)
    public ResponseEntity<School> createSchool (@RequestBody School school) {
        ServiceCall<School> createServiceCall = schoolService.create(school);

        return ResponseEntity
                .status(createServiceCall.getStatusCode())
                .body(createServiceCall.result());
    }

    @RequestMapping(method = GET)
    public ResponseEntity<List<School>> getSchools () {
        ServiceCall<List<School>> serviceCall = schoolService.find();

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = GET, value = "/{schoolId}")
    public ResponseEntity<School> getSchool (@PathVariable(name = "schoolId") Integer schoolId) {
        ServiceCall<School> serviceCall = schoolService.find(schoolId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = PUT, value = "/{schoolId}")
    public ResponseEntity<School> updateSchool (@PathVariable(name = "schoolId") Integer schoolId,
                                                      @RequestBody School school) {
        ServiceCall<School> serviceCall = schoolService.update(schoolId, school);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = DELETE, value = "/{schoolId}")
    public ResponseEntity serviceCall (@PathVariable(name = "schoolId") Integer schoolId) {
        ServiceCall<Boolean> serviceCall = schoolService.delete(schoolId);

        return ResponseEntity
                .status(serviceCall.getStatusCode()).build();
    }
}
