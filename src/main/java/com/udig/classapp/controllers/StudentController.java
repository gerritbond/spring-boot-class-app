package com.udig.classapp.controllers;

import com.udig.classapp.models.Student;
import com.udig.classapp.services.ServiceCall;
import com.udig.classapp.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping(value = "class-api/v1/student")
public class StudentController {
    private final StudentService studentService;

    @Autowired
    public StudentController (final StudentService studentService) {
        this.studentService = studentService;
    }


    @RequestMapping(method = POST)
    public ResponseEntity<Student> createStudent (@RequestBody Student student) {
        ServiceCall<Student> serviceCall = studentService.create(student);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());

    }

    @RequestMapping(method = GET)
    public ResponseEntity<List<Student>> getStudents () {
        ServiceCall<List<Student>> serviceCall = studentService.find();

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());

    }

    @RequestMapping(method = GET, value = "/{studentId}")
    public ResponseEntity<Student> getStudent (@PathVariable(name = "studentId") Integer studentId) {
        ServiceCall<Student> serviceCall = studentService.find(studentId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());

    }

    @RequestMapping(method = PUT, value = "/{studentId}")
    public ResponseEntity<Student> updateStudent (@PathVariable(name = "studentId") Integer studentId,
                                                      @RequestBody Student student) {
        ServiceCall<Student> serviceCall = studentService.update(studentId, student);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());

    }

    @RequestMapping(method = DELETE, value = "/{studentId}")
    public ResponseEntity deleteStudent (@PathVariable(name = "studentId") Integer studentId) {
        ServiceCall<Boolean> serviceCall = studentService.delete(studentId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());

    }

    @RequestMapping(method = PUT, value = "/{studentId}/enroll/{classId}")
    public ResponseEntity enrollStudent (@PathVariable(name = "studentId") Integer studentId,
                                   @PathVariable(name = "classId") Integer classId) {
        ServiceCall<Boolean> serviceCall = studentService.enroll(studentId, classId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());

    }

    @RequestMapping(method = PUT, value = "/{studentId}/disenroll/{classId}")
    public ResponseEntity disenrollStudent (@PathVariable(name = "studentId") Integer studentId,
                                   @PathVariable(name = "classId") Integer classId) {
        ServiceCall<Boolean> serviceCall = studentService.disenroll(studentId, classId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());

    }

}
