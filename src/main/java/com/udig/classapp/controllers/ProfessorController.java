package com.udig.classapp.controllers;

import com.udig.classapp.models.Professor;
import com.udig.classapp.services.ProfessorService;
import com.udig.classapp.services.ServiceCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping(value = "class-api/v1/professor")
public class ProfessorController {
    private final ProfessorService professorService;

    @Autowired
    public ProfessorController (final ProfessorService professorService) {
        this.professorService = professorService;
    }

    @RequestMapping(method = POST)
    public ResponseEntity<Professor> createProfessor (@RequestBody Professor professor) {
        ServiceCall<Professor> serviceCall = professorService.create(professor);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = GET)
    public ResponseEntity<List<Professor>> getProfessors () {
        ServiceCall<List<Professor>> serviceCall = professorService.find();

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = GET, value = "/{professorId}")
    public ResponseEntity<Professor> getProfessor (@PathVariable(name = "professorId") Integer professorId) {
        ServiceCall<Professor> serviceCall = professorService.find(professorId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = PUT, value = "/{professorId}")
    public ResponseEntity<Professor> updateProfessor (@PathVariable(name = "professorId") Integer professorId,
                                                      @RequestBody Professor professor) {
        ServiceCall<Professor> serviceCall = professorService.update(professorId, professor);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }

    @RequestMapping(method = DELETE, value = "/{professorId}")
    public ResponseEntity deleteProfessor (@PathVariable(name = "professorId") Integer professorId) {
        ServiceCall<Boolean> serviceCall = professorService.delete(professorId);

        return ResponseEntity
                .status(serviceCall.getStatusCode())
                .body(serviceCall.result());
    }
}
