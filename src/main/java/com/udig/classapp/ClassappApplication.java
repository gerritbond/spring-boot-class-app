package com.udig.classapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.udig.classapp"})
@SpringBootApplication
public class ClassappApplication {
	public static void main(String[] args) {
		SpringApplication.run(ClassappApplication.class, args);
	}
}
