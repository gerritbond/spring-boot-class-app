package com.udig.classapp.services;

import java.util.List;

public interface ServiceCall<T> {
    List<String> getMessages();
    Integer getStatusCode();
    T result();
}
