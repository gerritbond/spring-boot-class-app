package com.udig.classapp.services;

import com.udig.classapp.models.Student;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService extends JooqService {
    private final ClassService classService;

    @Autowired
    public StudentService(DSLContext context, ClassService classService) {
        super(context);
        this.classService = classService;
    }

    public ServiceCall<Student> create(Student student) {
        return new ServiceCall<Student>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Student result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Student result() {
                if(run)
                    return result;

                run = true;
                result = null;

                Record savedSchoolRecord =
                        context.insertInto(STUDENTS)
                                .columns(
                                        STUDENTS.FIRST_NAME,
                                        STUDENTS.MIDDLE_NAME,
                                        STUDENTS.LAST_NAME,
                                        STUDENTS.ADDRESS1,
                                        STUDENTS.ADDRESS2,
                                        STUDENTS.CREDITS,
                                        STUDENTS.SCHOOL)
                                .values(
                                        student.getFirstName(),
                                        student.getMiddleName(),
                                        student.getLastName(),
                                        student.getAddress1(),
                                        student.getAddress2(),
                                        student.getCredits(),
                                        student.getSchoolId())
                                .returning().fetchOne();

                if (savedSchoolRecord == null) {
                    statusCode = 500;
                    messages.add(String.format("Cannot persist record: %s", student.toString()));
                    return null;
                }

                result = find(savedSchoolRecord.get(STUDENTS.STUDENT_ID)).result();

                return result;
            }
        };

    }

    public ServiceCall<List<Student>> find() {
        return new ServiceCall<List<Student>>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private List<Student> result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public List<Student> result() {
                if(run)
                    return result;

                result = null;
                run = true;

                List<Student> students = new ArrayList<>();
                Result<?> result = context
                        .select(
                                STUDENTS.STUDENT_ID,
                                STUDENTS.FIRST_NAME,
                                STUDENTS.MIDDLE_NAME,
                                STUDENTS.LAST_NAME,
                                STUDENTS.ADDRESS1,
                                STUDENTS.ADDRESS2,
                                STUDENTS.CREDITS,
                                STUDENTS.SCHOOL,
                                SCHOOLS.SCHOOL_ID,
                                SCHOOLS.NAME)
                        .from(STUDENTS)
                        .join(SCHOOLS).on(STUDENTS.SCHOOL.eq(SCHOOLS.SCHOOL_ID))
                        .fetch();

                for(Record record : result)
                    students.add(new Student(record));

                this.result = students;
                return students;
            }
        };
    }

    public ServiceCall<Student> find(Integer studentId) {
        return new ServiceCall<Student>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Student result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Student result() {
                if(run)
                    return result;

                result = null;
                run = true;

                Record result = context
                        .select(
                                STUDENTS.STUDENT_ID,
                                STUDENTS.FIRST_NAME,
                                STUDENTS.MIDDLE_NAME,
                                STUDENTS.LAST_NAME,
                                STUDENTS.ADDRESS1,
                                STUDENTS.ADDRESS2,
                                STUDENTS.CREDITS,
                                STUDENTS.SCHOOL,
                                SCHOOLS.SCHOOL_ID,
                                SCHOOLS.NAME)
                        .from(STUDENTS)
                        .join(SCHOOLS).on(STUDENTS.SCHOOL.eq(SCHOOLS.SCHOOL_ID))
                        .where(STUDENTS.STUDENT_ID.eq(studentId))
                        .fetchOne();

                if(result != null) {
                    this.result = new Student(result);
                } else {
                    messages.add("No such student found.");
                    statusCode = 404;
                }

                return this.result;
            }
        };
    }

    public ServiceCall<Student> update(Integer studentId, Student student) {
        return new ServiceCall<Student>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Student result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Student result() {
                if(run)
                    return result;

                run = true;
                result = null;

                student.setStudentId(studentId);

                Integer recordCount =
                        context.selectCount()
                                .from(STUDENTS)
                                .where(STUDENTS.STUDENT_ID.eq(studentId))
                                .fetchOne().value1();

                if(recordCount == 0) {
                    messages.add(String.format("No record to update given student id %s", student));
                    statusCode = 404;
                    return null;
                }

                Integer updatedRecordCount =
                        context.update(STUDENTS)
                                .set(STUDENTS.FIRST_NAME, student.getFirstName())
                                .set(STUDENTS.MIDDLE_NAME, student.getMiddleName())
                                .set(STUDENTS.LAST_NAME, student.getLastName())
                                .set(STUDENTS.ADDRESS1, student.getAddress1())
                                .set(STUDENTS.ADDRESS2, student.getAddress2())
                                .set(STUDENTS.CREDITS, student.getCredits())
                                .set(STUDENTS.SCHOOL, student.getSchoolId())
                                .where(STUDENTS.STUDENT_ID.eq(studentId))
                                .execute();


                Student updatedStudent = find(studentId).result();

                if (updatedRecordCount > 0) {
                    result = updatedStudent;
                    return updatedStudent;
                }

                messages.add(String.format("Unable to persist changes for record %s", student.toString()));
                statusCode = 409;

                return null;
            }
        };
    }

    public ServiceCall<Boolean> delete(Integer studentId) {
        return new ServiceCall<Boolean>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Boolean run = false;
            private Boolean result;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Boolean result() {
                if(run)
                    return result;

                result = false;
                run = true;

                Integer deletedRecords =
                        context.deleteFrom(STUDENTS)
                                .where(STUDENTS.STUDENT_ID.eq(studentId))
                                .execute();

                if(deletedRecords == 0) {
                    messages.add(String.format("No such student given by %s", studentId));
                    statusCode = 404;
                }

                result = deletedRecords > 0;
                return result;
            }
        };
    }

    public ServiceCall<Boolean> enroll(Integer studentId, Integer classId) {
        return new ServiceCall<Boolean>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Boolean result;
            private Boolean run = false;


            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Boolean result() {
                if(run)
                    return result;

                run = true;
                result = false;

                Boolean studentExists = find(studentId).getStatusCode().equals(200);
                Boolean classExists = classService.find(classId).getStatusCode().equals(200);

                if(! (studentExists && classExists)) {
                    statusCode = 404;

                    if(!studentExists)
                        messages.add(String.format("Cannot find student by id, %s", studentId));

                    if(!classExists)
                        messages.add(String.format("Cannot find class by id, %s", classId));

                    return result;
                }

                if(context.insertInto(ENROLLMENT)
                        .columns(ENROLLMENT.CLASS, ENROLLMENT.STUDENT)
                        .values(classId, studentId)
                        .execute() > 0)
                    result = true;
                else
                    messages.add(String.format("Cannot enroll student %s into class %s", studentId, classId));

                return result;
            }
        };
    }

    public ServiceCall<Boolean> disenroll(Integer studentId, Integer classId) {
        return new ServiceCall<Boolean>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Boolean result;
            private Boolean run = false;


            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Boolean result() {
                if(run)
                    return result;

                run = true;
                result = false;

                if(context.deleteFrom(ENROLLMENT)
                        .where(ENROLLMENT.CLASS.eq(classId), ENROLLMENT.STUDENT.eq(studentId))
                        .execute() > 0)
                    result = true;
                else
                    messages.add(String.format("Cannot disenroll student %s into class %s", studentId, classId));

                return result;
            }
        };
    }
}
