package com.udig.classapp.services;

import com.udig.classapp.models.Professor;
import com.udig.classapp.models.School;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProfessorService extends JooqService {

    @Autowired
    public ProfessorService(DSLContext context) {
        super(context);
    }

    public ServiceCall<Boolean> delete(Integer professorId) {
        return new ServiceCall<Boolean>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Boolean run = false;
            private Boolean result;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Boolean result() {
                if(run)
                    return result;

                result = false;
                run = true;

                Integer deletedRecords =
                        context.deleteFrom(PROFESSORS)
                                .where(PROFESSORS.PROFESSOR_ID.eq(professorId))
                                .execute();

                if(deletedRecords == 0) {
                    messages.add(String.format("No such professor given by %s", professorId));
                    statusCode = 404;
                }

                result = deletedRecords > 0;
                return result;
            }
        };
    }

    public ServiceCall<List<Professor>> find() {
        return new ServiceCall<List<Professor>>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private List<Professor> result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public List<Professor> result() {
                if(run)
                    return result;

                result = null;
                run = true;

                List<Professor> professors = new ArrayList<>();
                Result<?> result = context
                        .select(
                                PROFESSORS.PROFESSOR_ID,
                                PROFESSORS.FIRST_NAME,
                                PROFESSORS.MIDDLE_NAME,
                                PROFESSORS.LAST_NAME,
                                PROFESSORS.SCHOOL_ID,
                                PROFESSORS.TITLE,
                                SCHOOLS.SCHOOL_ID,
                                SCHOOLS.NAME)
                        .from(PROFESSORS)
                        .join(SCHOOLS).on(PROFESSORS.SCHOOL_ID.eq(SCHOOLS.SCHOOL_ID))
                        .fetch();

                for(Record record : result)
                    professors.add(new Professor(record));

                this.result = professors;
                return professors;
            }
        };
    }

    public ServiceCall<Professor> find(Integer professorId) {
        return new ServiceCall<Professor>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Professor result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Professor result() {
                if(run)
                    return result;

                result = null;
                run = true;

                Professor professor = null;
                Record result = context
                        .select(
                                PROFESSORS.PROFESSOR_ID,
                                PROFESSORS.FIRST_NAME,
                                PROFESSORS.MIDDLE_NAME,
                                PROFESSORS.LAST_NAME,
                                PROFESSORS.SCHOOL_ID,
                                PROFESSORS.TITLE,
                                SCHOOLS.SCHOOL_ID,
                                SCHOOLS.NAME)
                        .from(PROFESSORS)
                        .join(SCHOOLS).on(PROFESSORS.SCHOOL_ID.eq(SCHOOLS.SCHOOL_ID))
                        .where(PROFESSORS.PROFESSOR_ID.eq(professorId))
                        .fetchOne();

                if(result != null) {
                    professor = new Professor(result);
                    this.result = professor;
                } else {
                    messages.add("No such professor found.");
                    statusCode = 404;
                }

                return professor;
            }
        };
    }

    public ServiceCall<Professor> update(Integer professorId, Professor professor) {
        return new ServiceCall<Professor>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Professor result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Professor result() {
                if(run)
                    return result;

                run = true;
                result = null;

                professor.setSchoolId(professorId);
                professor.setProfessorId(professorId);

                Integer recordCount =
                        context.selectCount()
                                .from(PROFESSORS)
                                .where(PROFESSORS.PROFESSOR_ID.eq(professorId))
                                .fetchOne().value1();

                if(recordCount == 0) {
                    messages.add(String.format("No record to update given professor id %s", professor));
                    statusCode = 404;
                    return null;
                }

                Integer updatedRecordCount =
                        context.update(PROFESSORS)
                                .set(PROFESSORS.FIRST_NAME, professor.getFirstName())
                                .set(PROFESSORS.MIDDLE_NAME, professor.getMiddleName())
                                .set(PROFESSORS.LAST_NAME, professor.getLastName())
                                .set(PROFESSORS.TITLE, professor.getTitle())
                                .set(PROFESSORS.SCHOOL_ID, professor.getSchoolId())
                                .where(PROFESSORS.PROFESSOR_ID.eq(professorId))
                                .execute();


                Professor updatedProfessor = find(professorId).result();

                if (updatedRecordCount > 0) {
                    result = updatedProfessor;
                    return updatedProfessor;
                }

                messages.add(String.format("Unable to persist changes for record %s", professor.toString()));
                statusCode = 409;

                return null;
            }
        };
    }

    public ServiceCall<Professor> create(Professor professor) {
        return new ServiceCall<Professor>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Professor result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Professor result() {
                if(run)
                    return result;

                run = true;
                result = null;

                Record savedSchoolRecord =
                        context.insertInto(PROFESSORS)
                                .columns(
                                        PROFESSORS.FIRST_NAME,
                                        PROFESSORS.MIDDLE_NAME,
                                        PROFESSORS.LAST_NAME,
                                        PROFESSORS.SCHOOL_ID,
                                        PROFESSORS.TITLE)
                                .values(
                                        professor.getFirstName(),
                                        professor.getMiddleName(),
                                        professor.getLastName(),
                                        professor.getSchoolId(),
                                        professor.getTitle())
                                .returning().fetchOne();

                if (savedSchoolRecord == null) {
                    statusCode = 500;
                    messages.add(String.format("Cannot persist record: %s", professor.toString()));
                    return null;
                }

                result = find(professor.getProfessorId()).result();

                return result;
            }
        };
    }
}
