package com.udig.classapp.services;

import com.udig.classapp.generated.db.tables.*;
import org.jooq.DSLContext;

public class JooqService {
    protected final DSLContext context;

    protected final static Classes CLASSES = new Classes();
    protected final static Enrollment ENROLLMENT = new Enrollment();
    protected final static Professors PROFESSORS = new Professors();
    protected final static Schools SCHOOLS = new Schools();
    protected final static Students STUDENTS = new Students();

    public JooqService (DSLContext context) {
        this.context = context;
    }
}
