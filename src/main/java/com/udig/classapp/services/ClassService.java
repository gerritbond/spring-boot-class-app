package com.udig.classapp.services;

import com.udig.classapp.models.Class;
import com.udig.classapp.models.Student;
import org.jooq.DSLContext;
import org.jooq.JoinType;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClassService extends JooqService {

    @Autowired
    public ClassService(DSLContext context) {
        super(context);
    }

    public ServiceCall<Class> create(Class newClass) {
        return new ServiceCall<Class>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Class result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Class result() {
                if(run)
                    return result;

                run = true;
                result = null;

                Record savedClassRecord =
                        context.insertInto(CLASSES)
                                .columns(
                                        CLASSES.COURSE_NAME,
                                        CLASSES.PROFESSOR,
                                        CLASSES.SCHOOL)
                                .values(
                                        newClass.getCourseName(),
                                        newClass.getProfessorId(),
                                        newClass.getSchoolId())
                                .returning().fetchOne();

                if (savedClassRecord == null) {
                    statusCode = 500;
                    messages.add(String.format("Cannot persist record: %s", newClass.toString()));
                    return null;
                }

                result = find(savedClassRecord.get(CLASSES.CLASS_ID)).result();

                return result;
            }
        };
    }

    public ServiceCall<List<Class>> find() {
        return new ServiceCall<List<Class>>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private List<Class> result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public List<Class> result() {
                if(run)
                    return result;

                result = new ArrayList<>();
                run = true;

                Result<?> result = context
                        .select(
                                CLASSES.CLASS_ID,
                                CLASSES.COURSE_NAME,
                                CLASSES.PROFESSOR,
                                CLASSES.SCHOOL,
                                SCHOOLS.SCHOOL_ID,
                                SCHOOLS.NAME,
                                PROFESSORS.FIRST_NAME,
                                PROFESSORS.MIDDLE_NAME,
                                PROFESSORS.LAST_NAME,
                                PROFESSORS.TITLE,
                                PROFESSORS.PROFESSOR_ID,
                                SCHOOLS.NAME,
                                ENROLLMENT.CLASS,
                                ENROLLMENT.STUDENT,
                                STUDENTS.STUDENT_ID,
                                STUDENTS.FIRST_NAME,
                                STUDENTS.MIDDLE_NAME,
                                STUDENTS.LAST_NAME,
                                STUDENTS.ADDRESS1,
                                STUDENTS.ADDRESS2,
                                STUDENTS.CREDITS)
                        .from(CLASSES)
                        .join(SCHOOLS).on(CLASSES.SCHOOL.eq(SCHOOLS.SCHOOL_ID))
                        .join(PROFESSORS).on(CLASSES.PROFESSOR.eq(PROFESSORS.PROFESSOR_ID))
                        .join(ENROLLMENT, JoinType.LEFT_OUTER_JOIN).on(CLASSES.CLASS_ID.eq(ENROLLMENT.CLASS))
                        .join(STUDENTS, JoinType.LEFT_OUTER_JOIN).on(ENROLLMENT.STUDENT.eq(STUDENTS.STUDENT_ID))
                        .fetch();

                Map<Integer, Class> enrollmentMap = buildEnrollmentMap(result);
                this.result.addAll(enrollmentMap.values());

                return this.result;
            }
        };
    }

    public ServiceCall<List<Class>> findBySchool(Integer schoolId) {
        return new ServiceCall<List<Class>>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private List<Class> result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public List<Class> result() {
                if(run)
                    return result;

                result = new ArrayList<>();
                run = true;

                Result<?> result = context
                        .select(
                                CLASSES.CLASS_ID,
                                CLASSES.COURSE_NAME,
                                CLASSES.PROFESSOR,
                                CLASSES.SCHOOL,
                                SCHOOLS.SCHOOL_ID,
                                SCHOOLS.NAME,
                                PROFESSORS.FIRST_NAME,
                                PROFESSORS.MIDDLE_NAME,
                                PROFESSORS.LAST_NAME,
                                PROFESSORS.TITLE,
                                PROFESSORS.PROFESSOR_ID,
                                SCHOOLS.NAME,
                                ENROLLMENT.CLASS,
                                ENROLLMENT.STUDENT,
                                STUDENTS.STUDENT_ID,
                                STUDENTS.FIRST_NAME,
                                STUDENTS.MIDDLE_NAME,
                                STUDENTS.LAST_NAME,
                                STUDENTS.ADDRESS1,
                                STUDENTS.ADDRESS2,
                                STUDENTS.CREDITS)
                        .from(CLASSES)
                        .join(SCHOOLS).on(CLASSES.SCHOOL.eq(SCHOOLS.SCHOOL_ID))
                        .join(PROFESSORS).on(CLASSES.PROFESSOR.eq(PROFESSORS.PROFESSOR_ID))
                        .join(ENROLLMENT, JoinType.LEFT_OUTER_JOIN).on(CLASSES.CLASS_ID.eq(ENROLLMENT.CLASS))
                        .join(STUDENTS, JoinType.LEFT_OUTER_JOIN).on(ENROLLMENT.STUDENT.eq(STUDENTS.STUDENT_ID))
                        .where(CLASSES.SCHOOL.eq(schoolId))
                        .fetch();

                Map<Integer, Class> enrollmentMap = buildEnrollmentMap(result);
                this.result.addAll(enrollmentMap.values());

                return this.result;
            }
        };
    }

    public ServiceCall<List<Class>> findByProfessor(Integer professorId) {
        return new ServiceCall<List<Class>>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private List<Class> result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public List<Class> result() {
                if(run)
                    return result;

                result = new ArrayList<>();
                run = true;

                Result<?> result = context
                        .select(
                                CLASSES.CLASS_ID,
                                CLASSES.COURSE_NAME,
                                CLASSES.PROFESSOR,
                                CLASSES.SCHOOL,
                                SCHOOLS.SCHOOL_ID,
                                SCHOOLS.NAME,
                                PROFESSORS.FIRST_NAME,
                                PROFESSORS.MIDDLE_NAME,
                                PROFESSORS.LAST_NAME,
                                PROFESSORS.TITLE,
                                PROFESSORS.PROFESSOR_ID,
                                SCHOOLS.NAME,
                                ENROLLMENT.CLASS,
                                ENROLLMENT.STUDENT,
                                STUDENTS.STUDENT_ID,
                                STUDENTS.FIRST_NAME,
                                STUDENTS.MIDDLE_NAME,
                                STUDENTS.LAST_NAME,
                                STUDENTS.ADDRESS1,
                                STUDENTS.ADDRESS2,
                                STUDENTS.CREDITS)
                        .from(CLASSES)
                        .join(SCHOOLS).on(CLASSES.SCHOOL.eq(SCHOOLS.SCHOOL_ID))
                        .join(PROFESSORS).on(CLASSES.PROFESSOR.eq(PROFESSORS.PROFESSOR_ID))
                        .join(ENROLLMENT, JoinType.LEFT_OUTER_JOIN).on(CLASSES.CLASS_ID.eq(ENROLLMENT.CLASS))
                        .join(STUDENTS, JoinType.LEFT_OUTER_JOIN).on(ENROLLMENT.STUDENT.eq(STUDENTS.STUDENT_ID))
                        .where(CLASSES.PROFESSOR.eq(professorId))
                        .fetch();


                Map<Integer, Class> enrollmentMap = buildEnrollmentMap(result);
                this.result.addAll(enrollmentMap.values());

                return this.result;
            }
        };
    }

    public ServiceCall<Class> update(Integer classId, Class updatedClass) {
        return new ServiceCall<Class>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Class result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Class result() {
                if(run)
                    return result;

                run = true;
                result = null;

                updatedClass.setClassId(classId);

                Integer recordCount =
                        context.selectCount()
                                .from(CLASSES)
                                .where(CLASSES.CLASS_ID.eq(classId))
                                .fetchOne().value1();

                if(recordCount == 0) {
                    messages.add(String.format("No record to update given class id %s", classId));
                    statusCode = 404;
                    return null;
                }

                Integer updatedRecordCount =
                        context.update(CLASSES)
                                .set(CLASSES.COURSE_NAME, updatedClass.getCourseName())
                                .set(CLASSES.PROFESSOR, updatedClass.getProfessorId())
                                .set(CLASSES.SCHOOL, updatedClass.getSchoolId())
                                .where(CLASSES.CLASS_ID.eq(classId))
                                .execute();


                Class updatedClassModel = find(classId).result();

                if (updatedRecordCount > 0) {
                    result = updatedClassModel;
                    return result;
                }

                messages.add(String.format("Unable to persist changes for record %s", updatedClass.toString()));
                statusCode = 409;

                return null;
            }
        };
    }

    public ServiceCall<Class> find(Integer classId) {
        return new ServiceCall<Class>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Class result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Class result() {
                if(run)
                    return result;

                result = null;
                run = true;

                Result<?> result = context
                        .select(
                                CLASSES.CLASS_ID,
                                CLASSES.COURSE_NAME,
                                CLASSES.PROFESSOR,
                                CLASSES.SCHOOL,
                                SCHOOLS.SCHOOL_ID,
                                SCHOOLS.NAME,
                                PROFESSORS.FIRST_NAME,
                                PROFESSORS.MIDDLE_NAME,
                                PROFESSORS.LAST_NAME,
                                PROFESSORS.TITLE,
                                PROFESSORS.PROFESSOR_ID,
                                SCHOOLS.NAME,
                                ENROLLMENT.CLASS,
                                ENROLLMENT.STUDENT,
                                STUDENTS.STUDENT_ID,
                                STUDENTS.FIRST_NAME,
                                STUDENTS.MIDDLE_NAME,
                                STUDENTS.LAST_NAME,
                                STUDENTS.ADDRESS1,
                                STUDENTS.ADDRESS2,
                                STUDENTS.CREDITS)
                        .from(CLASSES)
                        .join(SCHOOLS).on(CLASSES.SCHOOL.eq(SCHOOLS.SCHOOL_ID))
                        .join(PROFESSORS).on(CLASSES.PROFESSOR.eq(PROFESSORS.PROFESSOR_ID))
                        .join(ENROLLMENT, JoinType.LEFT_OUTER_JOIN).on(CLASSES.CLASS_ID.eq(ENROLLMENT.CLASS))
                        .join(STUDENTS, JoinType.LEFT_OUTER_JOIN).on(ENROLLMENT.STUDENT.eq(STUDENTS.STUDENT_ID))
                        .where(CLASSES.CLASS_ID.eq(classId))
                        .fetch();

                Map<Integer, Class> enrollmentMap = buildEnrollmentMap(result);

                if(! enrollmentMap.containsKey(classId)) {
                    messages.add("No such class found.");
                    statusCode = 404;
                }

                this.result = enrollmentMap.get(classId);
                return this.result;
            }
        };
    }

    public ServiceCall<Boolean> delete(Integer classId) {
        return new ServiceCall<Boolean>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Boolean run = false;
            private Boolean result;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Boolean result() {
                if(run)
                    return result;

                result = false;
                run = true;

                Integer deletedRecords =
                        context.deleteFrom(CLASSES)
                                .where(CLASSES.CLASS_ID.eq(classId))
                                .execute();

                if(deletedRecords == 0) {
                    messages.add(String.format("No such class given by %s", classId));
                    statusCode = 404;
                }

                result = deletedRecords > 0;
                return result;
            }
        };
    }

    private Map<Integer, Class> buildEnrollmentMap (Result<?> result) {
        Map<Integer, Class> enrollmentMap = new HashMap<>();
        for(Record record : result) {
            Class classInstance = new Class(record);
            if(enrollmentMap.containsKey(classInstance.getClassId()))
                enrollmentMap.get(classInstance.getClassId()).merge(classInstance);
            else
                enrollmentMap.put(classInstance.getClassId(), classInstance);
        }
        return enrollmentMap;
    }
}
