package com.udig.classapp.services;

import com.udig.classapp.models.School;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SchoolService extends JooqService {
    @Autowired
    public SchoolService(DSLContext context) {
        super(context);
    }

    public ServiceCall<Boolean> delete(final Integer schoolId) {
        return new ServiceCall<Boolean>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private Boolean run;
            private Boolean result;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public Boolean result() {
                if(run)
                    return result;

                result = false;
                Boolean deleted = false;
                run = true;

                Integer deletedRecords =
                        context.deleteFrom(SCHOOLS)
                                .where(SCHOOLS.SCHOOL_ID.eq(schoolId))
                                .execute();

                if(deletedRecords == 0) {
                    messages.add(String.format("No such school given by %s", schoolId));
                    statusCode = 404;
                }

                result = deletedRecords > 0;
                return result;
            }
        };
    }

    public ServiceCall<School> find(final Integer schoolId) {
        return new ServiceCall<School>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private School result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public School result() {
                if(run)
                    return result;

                result = null;
                run = true;

                School school = null;
                Record result = context
                        .select(SCHOOLS.fields())
                        .from(SCHOOLS)
                        .where(SCHOOLS.SCHOOL_ID.eq(schoolId))
                        .fetchOne();

                if(result != null) {
                    school = new School(result);
                    this.result = school;
                } else {
                    messages.add("No such school found.");
                    statusCode = 404;
                }

                return school;
            }
        };
    }

    public ServiceCall<List<School>> find() {
        return new ServiceCall<List<School>>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private List<School> result;
            private Boolean run = false;


            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                return statusCode;
            }

            @Override
            public List<School> result() {
                if(run)
                    return result;

                result = new ArrayList<>();
                run = true;

                List<School> schools = new ArrayList<>();
                Result<Record> result = context
                                            .select(SCHOOLS.fields())
                                            .from(SCHOOLS)
                                            .fetch();

                if(result.isNotEmpty())
                    for(Record record : result)
                        schools.add(new School(record));

                this.result = schools;
                return schools;
            }
        };
    }

    public ServiceCall<School> update(Integer schoolId, School school) {
        return new ServiceCall<School>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private School result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public School result() {
                if(run)
                    return result;

                run = true;
                result = null;

                school.setSchoolId(schoolId);

                Integer recordCount =
                        context.selectCount()
                        .from(SCHOOLS)
                        .where(SCHOOLS.SCHOOL_ID.eq(schoolId))
                        .fetchOne().value1();

                if(recordCount == 0) {
                    messages.add(String.format("No record to update given school id %s", schoolId));
                    statusCode = 404;
                    return null;
                }

                Integer updatedRecordCount =
                        context.update(SCHOOLS)
                        .set(SCHOOLS.NAME, school.getName())
                        .where(SCHOOLS.SCHOOL_ID.eq(schoolId))
                        .execute();

                if (updatedRecordCount > 0) {
                    result = school;
                    return school;
                }

                messages.add(String.format("Unable to persist changes for record %s", school.toString()));
                statusCode = 409;

                return null;
            }
        };
    }

    public ServiceCall<School> create(School school) {
        return new ServiceCall<School>() {
            private final List<String> messages = new ArrayList<>();
            private Integer statusCode = 200;
            private School result;
            private Boolean run = false;

            @Override
            public List<String> getMessages() {
                if(!run)
                    result();
                return messages;
            }

            @Override
            public Integer getStatusCode() {
                if(!run)
                    result();
                return statusCode;
            }

            @Override
            public School result() {
                if(run)
                    return result;

                run = true;
                result = null;

                Integer recordCount =
                        context.selectCount()
                                .from(SCHOOLS)
                                .where(SCHOOLS.NAME.eq(school.getName()))
                                .fetchOne().value1();

                if(recordCount != 0) {
                    messages.add(String.format("Existing record already exists with name %s", school.getName()));
                    statusCode = 409;
                    return null;
                }

                Record savedSchoolRecord =
                        context.insertInto(SCHOOLS)
                                .columns(SCHOOLS.NAME)
                                .values(school.getName())
                                .returning().fetchOne();

                if (savedSchoolRecord == null) {
                    statusCode = 500;
                    messages.add(String.format("Cannot persist record: %s", school.toString()));
                    return null;
                }

                result = new School(savedSchoolRecord);

                return result;
            }
        };
    }
}
