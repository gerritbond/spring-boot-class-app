package com.udig.classapp.models;

import com.udig.classapp.generated.db.tables.Schools;
import org.jooq.Record;

public class School implements Model {
    private final static Schools SCHOOLS = new Schools();

    private Integer schoolId;
    private String name;

    public School (Record r) {
        this.name = r.get(SCHOOLS.NAME);
        this.schoolId = r.get(SCHOOLS.SCHOOL_ID);
    }

    School () {

    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
