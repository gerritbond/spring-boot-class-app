package com.udig.classapp.models;

import com.udig.classapp.generated.db.tables.Classes;
import com.udig.classapp.generated.db.tables.Professors;
import com.udig.classapp.generated.db.tables.Schools;
import com.udig.classapp.generated.db.tables.Students;
import org.jooq.Record;

import java.util.ArrayList;
import java.util.List;

public class Class implements Model {
    private final static Classes CLASSES = new Classes();
    private final static Professors PROFESSORS = new Professors();
    private final static Schools SCHOOLS = new Schools();
    private final static Students STUDENTS = new Students();

    private Integer classId;
    private String courseName;

    private Integer professorId;
    private Professor professor;

    private Integer schoolId;
    private School school;

    private List<Student> students;

    public Class (Record r) {
        this.classId = r.get(CLASSES.CLASS_ID);
        this.courseName = r.get(CLASSES.COURSE_NAME);

        this.professorId = r.get(CLASSES.PROFESSOR);
        if(r.get(PROFESSORS.PROFESSOR_ID) != null)
            this.professor = new Professor(r);

        this.schoolId = r.get(CLASSES.SCHOOL);
        if(r.get(SCHOOLS.SCHOOL_ID) != null) {
            this.school = new School(r);

        }

        if(r.get(STUDENTS.STUDENT_ID) != null) {
            students = new ArrayList<>();
            students.add(new Student(r));
        }
    }

    Class () {

    }

    public Class merge (Object o) {
        if(o instanceof Class && this.getClassId().equals(((Class) o).getClassId()))
            this.getStudents().addAll(((Class) o).getStudents());

        return this;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Professor getProfessor() {
        return professor;
    }

    public Integer getProfessorId() {
        return professorId;
    }

    public void setProfessorId(Integer professorId) {
        this.professorId = professorId;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public School getSchool() {
        return school;
    }

    public List<Student> getStudents() {
        if(students == null)
            students = new ArrayList<>();
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
