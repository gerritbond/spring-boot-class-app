package com.udig.classapp.models;

import com.udig.classapp.generated.db.tables.Schools;
import com.udig.classapp.generated.db.tables.Students;
import org.jooq.Record;

public class Student implements Model {
    private static final Students STUDENTS = new Students();
    private static final Schools SCHOOLS = new Schools();

    private Integer studentId;

    private String firstName;
    private String lastName;
    private String middleName;
    private String address1;
    private String address2;
    private Integer credits;

    private Integer schoolId;
    private School school;

    public Student (Record r) {
        studentId = r.get(STUDENTS.STUDENT_ID);

        firstName = r.get(STUDENTS.FIRST_NAME);
        lastName = r.get(STUDENTS.LAST_NAME);
        middleName = r.get(STUDENTS.MIDDLE_NAME);

        address1 = r.get(STUDENTS.ADDRESS1);
        address2 = r.get(STUDENTS.ADDRESS2);

        credits = r.get(STUDENTS.CREDITS);

        schoolId = r.get(STUDENTS.SCHOOL);

        if(r.get(SCHOOLS.SCHOOL_ID) != null)
            school = new School(r);
    }

    Student () {

    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public School getSchool() {
        return school;
    }
}
