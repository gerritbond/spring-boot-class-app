package com.udig.classapp.models;

import com.udig.classapp.generated.db.tables.Professors;
import com.udig.classapp.generated.db.tables.Schools;
import org.jooq.Record;

public class Professor implements Model {
    private static final Professors PROFESSORS = new Professors();
    private static final Schools SCHOOLS = new Schools();

    private Integer professorId;

    private String firstName;
    private String middleName;
    private String lastName;
    private String title;

    private Integer schoolId;
    private School school;

    public Professor (Record r) {
        this.firstName = r.get(PROFESSORS.FIRST_NAME);
        this.middleName = r.get(PROFESSORS.MIDDLE_NAME);
        this.lastName = r.get(PROFESSORS.LAST_NAME);
        this.title = r.get(PROFESSORS.TITLE);

        this.professorId = r.get(PROFESSORS.PROFESSOR_ID);
        this.schoolId = r.get(PROFESSORS.SCHOOL_ID);

        if(r.get(SCHOOLS.SCHOOL_ID) != null && r.get(PROFESSORS.PROFESSOR_ID).equals(r.get(SCHOOLS.SCHOOL_ID))) {
            this.schoolId = r.get(SCHOOLS.SCHOOL_ID);
            this.school = new School(r);
        }
    }

    Professor () {

    }

    public Integer getProfessorId() {
        return professorId;
    }

    public void setProfessorId(Integer professorId) {
        this.professorId = professorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public School getSchool() {
        return school;
    }
}
