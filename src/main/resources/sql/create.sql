﻿CREATE USER rdsp_admin WITH PASSWORD 'N9V4431fdsf32ZsL7e';
ALTER USER rdsp_admin WITH SUPERUSER;

CREATE DATABASE ramdev_sample_project
WITH
OWNER = 'rdsp_admin'
ENCODING = 'utf-8'
CONNECTION LIMIT = 40;

CREATE TABLE IF NOT EXISTS users (
	user_id SERIAL NOT NULL,
    username VARCHAR(255) NOT NULL,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    pass VARCHAR(255),
    salt VARCHAR(255),
    hash TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT users_pkey PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS schools (
	school_id SERIAL NOT NULL,
    name VARCHAR (1024) NOT NULL,
    CONSTRAINT school_pkey PRIMARY KEY (school_id)
);

CREATE TABLE IF NOT EXISTS professors (
	professor_id SERIAL NOT NULL,
    school_id INTEGER NOT NULL REFERENCES schools(school_id),
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    middle_name VARCHAR(255) NOT NULL,
    title VARCHAR(255) NOT NULL,
    CONSTRAINT professor_pkey PRIMARY KEY (professor_id)
);

CREATE TABLE IF NOT EXISTS classes (
	class_id SERIAL NOT NULL,
    course_name VARCHAR(1024) NOT NULL,
    professor INTEGER NOT NULL REFERENCES professors(professor_id),
    school INTEGER NOT NULL REFERENCES schools(school_id),
 	CONSTRAINT class_pkey PRIMARY KEY (class_id)
);

CREATE TABLE IF NOT EXISTS students (
	student_id SERIAL NOT NULL,
    first_name VARCHAR (255) NOT NULL,
    middle_name VARCHAR (255) NOT NULL,
    last_name VARCHAR (255) NOT NULL,
    address1 VARCHAR (255) NOT NULL,
    address2 VARCHAR (255) NOT NULL,
	  school INTEGER NOT NULL REFERENCES schools(school_id),
    credits INTEGER NOT NULL DEFAULT (0),
    CONSTRAINT student_pkey PRIMARY KEY (student_id)
);

CREATE TABLE IF NOT EXISTS enrollment (
    enrollment_id SERIAL NOT NULL,
	class INTEGER NOT NULL REFERENCES classes(class_id),
    student INTEGER NOT NULL REFERENCES students(student_id),
    CONSTRAINT enrollment_pkey PRIMARY KEY (enrollment_id)
);